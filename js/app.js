let menFashion = $('.men-fashion');
menFashion.hide();

let womenFashion = $('.women-fashion');
womenFashion.hide();

let banner = $('.banner');

function showMen() {
	banner.hide();
	menFashion.show();
	womenFashion.hide();

	// fetch mens fashion from api
	$.ajax({
		url: 'http://127.0.0.1:3333/men',
		success: function(result) {
			console.log(result);
		},
	});
}

function showWomen() {
	banner.hide();
	womenFashion.show();
	menFashion.hide();
	let result;
	let list;

	// fetch womens fashion from api

	$.ajax({
		url: 'http://127.0.0.1:3333/women',
		success: function(data) {
			result = data.women_fashion;
			list = '<ul>';
			result.forEach(obj => {
				for (let key in obj) {
					list = list + '<li>' + obj[key] + '</li>';
				}
			});
			list += '</ul>';
			$('.womenresult').replaceWith(list);
			console.log(list);
		},
	});
}
